import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import store from './store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './components/Menu';
import Checkout from './components/Checkout';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faIgloo } from '@fortawesome/free-solid-svg-icons';

library.add(faIgloo);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Router>
            <Switch>
              <Route exact path="/restaurant/checkout" component={Checkout} />
              <Route path="/restaurant/:id" component={Menu} />
              <Route path="/checkout/" component={Checkout} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
