import globalConst from "../helpers/global";
export const FETCH_PRODUCTS_BEGIN = "FETCH_PRODUCTS_BEGIN";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_FAILURE = "FETCH_PRODUCTS_FAILURE";
export const TOGGLE_MODAL = "TOGGLE_MODAL";
export const ADD_QUANTITY = "ADD_QUANTITY";
export const REMOVE_QUANTITY = "REMOVE_QUANTITY";
export const ADD_TO_CART = "ADD_TO_CART";
export const GET_CART = "GET_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const TOGGLE_DELIVERY_MODAL = "TOGGLE_DELIVERY_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";

//restaurant
export const GET_RESTAURANT_BEGIN = "GET_RESTAURANT_BEGIN";
export const GET_RESTAURANT_SUCCESS = "GET_RESTAURANT_SUCCESS";
export const GET_RESTAURANT_FAILURE = "GET_RESTAURANT_FAILURE";

///order
export const CHECK_USER = "CHECK_USER";
export const CHECK_USER_BEGIN = "CHECK_USER_BEGIN";
export const CHECK_USER_SUCCESS = "CHECK_USER_SUCCESS";
export const CHECK_USER_FAILURE = "CHECK_USER_FAILURE";
export const USER_FORM_CHANGE = "USER_FORM_CHANGE";

export const USER_DELIVER_FORM_CHANGE = "USER_DELIVER_FORM_CHANGE";
export const EDIT_USER = "EDIT_USER";
export const RESET_ORDER = "RESET_ORDER";

export const ONLINE_ORDER_BEGIN = "ONLINE_ORDER_BEGIN";
export const ONLINE_ORDER_SUCCESS = "ONLINE_ORDER_SUCCESS";
export const ONLINE_ORDER_FAILURE = "ONLINE_ORDER_FAILURE";

export const CLEAR_STORAGE = "CLEAR_STORAGE";

export const CHANGE_INSTRUCTION = "CHANGE_INSTRUCTION";

export const toggleModal = product => ({
  type: TOGGLE_MODAL,
  payload: product
});

export const resetModal = () => ({
  type: CLOSE_MODAL
});

export const addQuantity = () => ({
  type: ADD_QUANTITY
});
export const removeQuantity = () => ({
  type: REMOVE_QUANTITY
});

export const addItems = () => ({
  type: ADD_TO_CART
});

export const getCart = () => ({
  type: GET_CART
  // cart: []
  // cart: ,
  // totalPrice: localStorage.getItem('totalPrice')
  //   ? JSON.parse(localStorage.getItem('totalPrice'))
  //   : 0,
  // totalQuantity: localStorage.getItem('totalQuantity')
  //   ? JSON.parse(localStorage.getItem('totalQuantity'))
  //   : 0
});

export const removeFromCart = id => ({
  type: REMOVE_FROM_CART,
  id
});

export const handleQuantityChange = value => ({
  type: CHANGE_QUANTITY,
  value
});

export const deliveryOptionsPopup = orderType => ({
  type: TOGGLE_DELIVERY_MODAL,
  orderType
});

export const fetchProductsBegin = () => ({
  type: FETCH_PRODUCTS_BEGIN
});

export const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: { products }
});

export const fetchProductsFailure = error => ({
  type: FETCH_PRODUCTS_FAILURE,
  payload: { error }
});

export function fetchProducts(id) {
  return dispatch => {
    dispatch(fetchProductsBegin());
    return fetch(`${globalConst.serverip}public/getpcateogories/${id}`)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchProductsSuccess(json.data));
        return json.data;
      })
      .catch(error => {
        localStorage.clear();
        dispatch(fetchProductsFailure(error));
      });
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

//res info

export const getRestaurantInfo = id => {
  return dispatch => {
    dispatch(getRestaurantBegin());
    return fetch(`${globalConst.serverip}public/getpbranchinfo/${id}`)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        console.log("res", json.data);
        dispatch(getRestaurantSuccess(json.data));
        return json.data;
      })
      .catch(error => {
        localStorage.clear();
        dispatch(getRestaurantFailure(error));
      });
  };
};

export const getRestaurantBegin = () => ({
  type: GET_RESTAURANT_BEGIN
});

export const getRestaurantSuccess = data => ({
  type: GET_RESTAURANT_SUCCESS,
  data
});

export const getRestaurantFailure = error => ({
  type: GET_RESTAURANT_FAILURE,
  error
});



