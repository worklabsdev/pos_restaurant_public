import React from "react";
import { ListGroup, ListGroupItem, Button, Card } from "reactstrap";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

const Cart = ({
  cart,
  totalPrice,
  totalQuantity,
  openDeliveryOptions,
  removeItemFromCart,
  placeOrder,
  location,
  active,
  button,
  driveoutCharge,
  orderType
}) => {
  console.log("CART CART", cart);
  return (
    <Card className="sidebar_element">
      <div className="total_product_element">
        <ul>
          <li>
            {/* {history.location.pathname === '/checkout/pickup' && (
              <Button
                color="success"
                disabled={!active || cart.length < 1}
                onClick={placeOrder}
              >
                Place Order
              </Button>
            )}
            {history.location.pathname !== '/checkout/pickup/otp' && (
              <Button
                color="success"
                disabled={cart.length < 1}
                onClick={openDeliveryOptions}
              >
                Continue
              </Button>
            )}
            {active && history.location.pathname === '/checkout/pickup/otp' && (
              <span style={{ textAlign: 'center', width: '100%' }}>
                Order Summary
              </span>
            )} */}
            {button === "Continue" && (
              <Button
                color="success"
                disabled={cart.length < 1}
                onClick={openDeliveryOptions}
              >
                Continue
              </Button>
            )}
            {button === "Place Order" &&
              (location.pathname === "/checkout/pickup" ||
                location.pathname === "/checkout/deliver") && (
                <Button
                  color="success"
                  disabled={!active || cart.length < 1}
                  onClick={placeOrder}
                >
                  Place Order
                </Button>
              )}
            {location.pathname === "/checkout/pickup/otp" && (
              <span style={{ textAlign: "center", width: "100%" }}>
                Order Summary
              </span>
            )}
          </li>
        </ul>
      </div>
      {cart.length < 1 && <span className="plate-image" />}
      <ListGroup className="">
        {cart.length > 0 &&
          cart.map((prod, index) => {
            return (
              <ListGroupItem key={prod.itemNo}>
                <span className="col-5" style={{ marginLeft: "-10px" }}>
                  {prod.name}
                  {location.pathname !== "/checkout/pickup/otp" && (
                    <>
                      <br />
                      <button
                        style={{ cursor: "pointer" }}
                        onClick={() => removeItemFromCart(prod._id)}
                        className="link-button"
                      >
                        <small>Remove</small>
                      </button>
                    </>
                  )}
                </span>
                <small className="col-3 cart-quantity">x {prod.quantity}</small>
                <span className="col-4" style={{ marginLeft: "21px" }}>
                  {" "}
                  Kr. <strong>{prod.totalPrice}</strong>
                </span>
              </ListGroupItem>
            );
          })}
        <span style={{ display: "flex", justifyContent: "space-around" }}>
          <span style={{ paddingRight: "100px" }}>Subtotal</span>
          <span>Kr. {(totalPrice / 1.15).toFixed(2)}</span>
        </span>
        <span style={{ display: "flex", justifyContent: "space-around" }}>
          <span style={{ paddingRight: "136px" }}>Tax</span>
          <span>Kr. {(totalPrice - totalPrice / 1.15).toFixed(2)}</span>
        </span>

        {(location.pathname === "/checkout/pickup/otp" ||
          location.pathname === "/checkout/deliver") &&
        orderType != "Take away" ? (
          <p>
            <span
              style={{
                display: "flex",
                justifyContent: "space-around",
                marginLeft: "-5px"
              }}
            >
              <span style={{ marginLeft: "0px" }}>
                Drive out Tax <b style={{ fontSize: "12px" }}>(25%)</b>
              </span>
              <span style={{ marginLeft: "12px" }}>
                Kr. {(driveoutCharge - (driveoutCharge * 100) / 125).toFixed(2)}
              </span>
            </span>
            <span
              style={{
                display: "flex",
                justifyContent: "space-around",
                marginLeft: "-1px"
              }}
            >
              <span style={{ marginLeft: "4px" }}>Drive out charges</span>
              <span style={{ marginLeft: "37px" }}>
                Kr. {((driveoutCharge * 100) / 125).toFixed(2)}
              </span>
            </span>
            <span className="subtotal-list">
              <span style={{ marginLeft: "-31px" }} className="subtotal">
                Subtotal <strong>({totalQuantity} Items)</strong>
              </span>
              <span style={{ marginLeft: "-36px" }} className="price">
                Kr. <strong>{totalPrice + driveoutCharge}</strong>
              </span>
            </span>
          </p>
        ) : (
          <p>
            <span className="subtotal-list">
              <span style={{ marginLeft: "-31px" }} className="subtotal">
                Subtotal <strong>({totalQuantity} Items)</strong>
              </span>
              <span style={{ marginLeft: "-36px" }} className="price">
                Kr. <strong>{totalPrice}</strong>
              </span>
            </span>
          </p>
        )}
      </ListGroup>
    </Card>
  );
};

Cart.propTypes = {
  cart: PropTypes.array,
  totalPrice: PropTypes.number,
  totalQuantity: PropTypes.number,
  removeItemFromCart: PropTypes.function
};

export default withRouter(Cart);
