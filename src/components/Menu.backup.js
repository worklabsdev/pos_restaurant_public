import React, { Component } from 'react';
import {
  ListGroup,
  ListGroupItem,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  // Card,
  // CardTitle,
  // CardText,
  Row,
  Col
} from 'reactstrap';
import { Container } from 'reactstrap';
import {} from 'reactstrap';
import globalConst from '../helpers/global';
import classnames from 'classnames';
import axios from 'axios';
import { get } from 'https';
import plate from '../assets/plate.png';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loaded: false,
      activeTab: '1',
      modal: false,
      localProduct: [],
      tempProducts: [],
      prodArr: [],
      editingId: '0',
      quantity: 1,
      cartInfo: {},
      totalQuantity: 0,
      totalAmount: 0
    };
  }

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  toggleModal = product => {
    console.log('teststststststs', this.state);
    if (this.state.modal === false) {
      this.setState({
        modal: !this.state.modal,
        editingId: product
      });
    } else {
      this.setState({ editingId: '0', modal: false });
    }
  };

  componentDidMount() {
    this.getCategories();
    this.getCartTotal();
    if (localStorage.getItem('localProd')) {
      this.setState({ prodArr: JSON.parse(localStorage.getItem('localProd')) });
    } else {
      this.setState({ prodArr: [] });
    }
  }

  IncrementItem = () => {
    this.setState({ quantity: this.state.quantity + 1 });
  };
  DecreaseItem = () => {
    if (this.state.quantity !== 0) {
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  getCategories() {
    console.log('srverip', globalConst.serverip);
    var id = this.props.match.params.id;
    fetch(globalConst.serverip + 'public/getpcateogories/' + id, {
      method: 'get'
    })
      .then(response => response.json())
      .then(response => {
        console.log('data', response);
        this.setState({ products: response.data, loaded: true });
      })
      .catch(error => {
        console.log(error);
      });
  }

  async removeItemFromCart(itemNo) {
    var cart = JSON.parse(localStorage.getItem('localProd'));
    await this.setState({ tempProducts: cart });
    if (this.state.tempProducts) {
      this.state.tempProducts.map((item, index) => {
        if (item.itemNo === itemNo) {
          var quantityLeft = this.state.totalQuantity - item.quantity;
          var amountLeft = this.state.totalAmount - item.totalPrice;

          this.setState({ totalQuantity: quantityLeft });
          this.setState({ totalAmount: amountLeft });
          this.state.tempProducts.splice(index, 1);
        }
      });
    }
    window.localStorage.localProd = JSON.stringify(this.state.tempProducts);
    this.setState({ prodArr: JSON.parse(localStorage.localProd) });
  }

  addItem(item) {
    this.setState({ editingId: item.itemNo });
    item.totalPrice = this.state.quantity * item.price;
    item.quantity = this.state.quantity;
    console.log('Immediate after ', this.state.prodArr);
    var it = JSON.stringify(item);
    console.log('this is it', it);
    let prodArr = this.state.prodArr;
    let alreadyInCart = false;
    alreadyInCart = prodArr.some((prod, i, arr) => {
      console.log('gg44', prod._id === item.id);
      if (prod.itemNo === item.itemNo) {
        // prod.quantity += item.quantity;
        // const quantity = prod.quantity + item.quantity;
        const newProdArr = arr.map(itemN => {
          if (itemN.itemNo === prod.itemNo) {
            itemN.quantity = prod.quantity + item.quantity;
            return itemN;
          }
          return itemN;
        });
        this.setState({ prodArr: newProdArr });
      }
    });

    // alreadyInCart = prodArr.some(prod => {
    //   if (prod._id === item._id) {
    //     console.log('gg prod', prod.quantity);
    //     console.log('gg item', item.quantity);
    //     prod.quantity += item.quantity;
    //   }
    //   return prod._id === item._id;
    // });

    // alreadyInCart = prodArr.some((prod, i, arr) => {
    //   if (prod._id === item._id) {
    //     console.log('gg prod', arr);
    //     console.log('gg item', item);
    //     prod.quantity += item.quantity;
    //     return true;
    //   }
    // });

    console.log('gg3', alreadyInCart);
    if (!alreadyInCart) {
      prodArr.push(item);
    }
    console.log('before setting state ptodArr', this.state.prodArr);
    this.setState({ prodArr });
    console.log('AFTER setting state ptodArr', this.state.prodArr);
    console.log('this is the prodARr', this.state.prodArr);
    window.localStorage.setItem(
      'localProd',
      JSON.stringify(this.state.prodArr)
    );
    var firstProd = JSON.parse(localStorage.getItem('localProd'));
    this.getCartTotal();
    this.setState({ quantity: 1 });
    // this.checkProductInState(item.itemNo);
    this.toggleModal();

    console.log('Local Sotrage data', firstProd[0].itemNo);
  }

  checkProductInLocalStorage(item_number) {
    var localItems = JSON.parse(localStorage.getItem('localProd'));
    console.log('localItems in check Products', localItems);
    if (localItems) {
      for (var i = 0; i < localItems.length; i++) {
        if (localItems[i].itemNo == item_number) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  checkProductInState(itemno) {
    // alert(itemno);
    console.log('item No', itemno);
    var items = this.state.prodArr;
    console.log('this is checkProduct', items);
    if (items) {
      console.log('you are in ');
      this.state.prodArr.map(item => {
        console.log('this is item ', item);
        if (item.itemNo === itemno) {
          console.log('this is item ', item.itemNo);
          console.log('you got true');
          return true;
        } else {
          console.log('you got false');
          return false;
        }
      });
    } else {
      return false;
    }
  }

  // checkProductInState = itemNo => {
  //   const itemsCart = this.state.prodArr;
  //   const items = this.state.products;
  //   itemsCart && itemsCart.map(item=> {
  //     console.log('itemNo', itemNo)
  //     console.log(item.itemNo === itemNo)
  //     return item.itemNo === itemNo
  //   })
  // }

  getCartTotal() {
    var firstProd = JSON.parse(localStorage.getItem('localProd'));
    console.log('cartPRODYCT', firstProd);
    var totalQuantity = 0,
      totalAmount = 0;
    if (firstProd) {
      firstProd.map(async (item, index) => {
        console.log('quantity', item.quantity);
        totalQuantity += item.quantity;
        totalAmount += item.totalPrice;
        console.log('this is state,', totalQuantity);
        console.log('this is state,', totalAmount);
      });
      // for (var t = 0; t < firstProd.length; t++) {
      //    totalQuantity += firstProd[t].quantity;
      //   console.log("this is state333333,", totalQuantity);
      //   totalAmount += firstProd[t].price;
      // }
      console.log('this is state,', totalQuantity);
      console.log('this is state,', totalAmount);
      this.setState({ totalQuantity: totalQuantity });
      this.setState({ totalAmount: totalAmount });
    }
  }

  handleQuantityChange(e) {
    this.setState({ quantity: e.target.value });
  }
  updateParticularItemInCart(itemNo) {
    console.log('this is item no', itemNo);
  }

  //  =========================================   start on JSX   ==================================================
  // ==============================================================================================================
  render() {
    console.log('RNDER FIRST STATE-----------', this.state);
    const { loaded, products } = this.state;
    if (!loaded) {
      return <div>Loading...</div>;
    } else {
      // console.log(localItems);
      return (
        <div>
          <Navbar color="light" light expand="md">
            <div className="container">
              <NavbarBrand href="/">POS</NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink href="/components/">Components</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="https://github.com/reactstrap/reactstrap">
                      GitHub
                    </NavLink>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Options
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>Option 1</DropdownItem>
                      <DropdownItem>Option 2</DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem>Reset</DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Collapse>
            </div>
          </Navbar>

          {/*  start of tabs */}
          <div className="container" style={{ marginTop: '30px' }}>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === '1'
                  })}
                  onClick={() => {
                    this.toggle('1');
                  }}
                >
                  Order
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === '2'
                  })}
                  onClick={() => {
                    this.toggle('2');
                  }}
                >
                  Photos
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent
              activeTab={this.state.activeTab}
              className="tab-main-element"
            >
              <TabPane tabId="1">
                <Container>
                  <Row>
                    <Col md="8">
                      {products.map((item, index) => {
                        return (
                          <div className="product-list-element" key={item._id}>
                            <h5>{item.categoryInfo[0].name}</h5>
                            <ListGroup>
                              {item.products.map((product, index) => {
                                return (
                                  <ListGroupItem
                                    style={{ textAlign: 'left' }}
                                    key={product.itemNo}
                                  >
                                    <img
                                      src="http://localhost:8080/uploads/icons/15420189083374.png"
                                      style={{ height: '40px' }}
                                    />
                                    <p className="Menustyle">
                                      {product.name}
                                      <span>Kr. {product.price}</span>
                                    </p>
                                    {/* <input type="Number" style={{float:'right',marginTop:'-20px'}}></input> */}
                                    {this.checkProductInState(
                                      product.itemNo
                                    ) ? (
                                      <p>Added</p>
                                    ) : (
                                      <Button
                                        onClick={() =>
                                          this.toggleModal(product)
                                        }
                                        outline
                                        color="success"
                                      >
                                        Add
                                      </Button>
                                    )}
                                  </ListGroupItem>
                                );
                              })}
                            </ListGroup>
                          </div>
                        );
                      })}
                    </Col>
                    <Col md="4">
                      <div className="sidebar_element">
                        {!this.state.prodArr && !this.state.prodArr.length && (
                          <img src={plate} />
                        )}
                        <ListGroup
                          className=""
                          style={{ position: 'absolute' }}
                        >
                          {localStorage.localProd &&
                            this.state.prodArr.map((prod, index) => {
                              return (
                                <ListGroupItem key={prod.itemNo}>
                                  <select
                                    value={prod.quantity}
                                    onChange={() =>
                                      this.updateParticularItemInCart(
                                        prod.itemNo
                                      )
                                    }
                                    className="form-control col-2"
                                  >
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                    <option>11</option>
                                  </select>
                                  <span className="col-6">
                                    {prod.name}
                                    <br />
                                    <a
                                      style={{ cursor: 'pointer' }}
                                      onClick={() =>
                                        this.removeItemFromCart(prod.itemNo)
                                      }
                                      className="remove_item"
                                    >
                                      Remove
                                    </a>
                                  </span>
                                  <span className="col-4">
                                    {' '}
                                    Kr. {prod.totalPrice}
                                  </span>
                                </ListGroupItem>
                              );
                            })}
                        </ListGroup>

                        <div className="total_product_element">
                          <ul>
                            <li>
                              <span className="subtotal">
                                Subtotal ({this.state.totalQuantity})
                              </span>
                              <span className="price">
                                kr. {this.state.totalAmount}
                              </span>
                            </li>
                            {/* <li>
                              <textarea placeholder="Search" />
                            </li> */}
                            <li>
                              <Button outline color="success">
                                Continue
                              </Button>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </TabPane>
              <TabPane tabId="2">
                <h2>Showing Menu </h2>
                <div className="Row">
                  <div className="colMd6">hi</div>
                  <div className="colMd6">there</div>
                </div>
              </TabPane>
            </TabContent>
          </div>
          {/* End of tabs */}

          <Modal
            isOpen={this.state.modal}
            toggle={this.toggleModal}
            className={this.props.className}
          >
            <ModalHeader toggle={this.toggleModal}>
              {this.state.editingId.name}
              <br />
              <span style={{ color: 'grey', fontSize: '15px' }}>
                Original Price : Kr. {this.state.editingId.price}
              </span>
            </ModalHeader>
            {/* <ModalBody>
              {" "}
              {this.state.editingId.itemNo}
              Lorem ipsum dolor sit amet
            </ModalBody> */}
            <ModalFooter>
              <div className="col-md-4">
                <div className="add_product">
                  <Button
                    onClick={this.DecreaseItem}
                    outline
                    color="secondary"
                    className="minus_btn"
                  >
                    -
                  </Button>
                  <input
                    className="form-control"
                    type="text"
                    value={this.state.quantity}
                    onChange={e => this.handleQuantityChange(e)}
                  />
                  <Button
                    onClick={this.IncrementItem}
                    outline
                    color="secondary"
                    className="add_btn"
                  >
                    +
                  </Button>
                </div>
              </div>

              <div className="col-md-8">
                <Button
                  color="success"
                  onClick={() => this.addItem(this.state.editingId)}
                  className="btn-block"
                >
                  Add {this.state.quantity} to cart{' '}
                  <span style={{ fontSize: '12px' }}>
                    [ Kr {this.state.editingId.price * this.state.quantity} ]
                  </span>
                </Button>{' '}
              </div>
            </ModalFooter>
          </Modal>
        </div>
      );
    }
  }
}

export default Menu;
