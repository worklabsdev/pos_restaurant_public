import { combineReducers } from 'redux';
import products from './productsReducer';
import order from './orderReducer';
import restaurant from './restaurantReducer';

export default combineReducers({ products, order, restaurant });
