import {
  FETCH_PRODUCTS_BEGIN,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  TOGGLE_MODAL,
  ADD_QUANTITY,
  REMOVE_QUANTITY,
  CHANGE_QUANTITY,
  ADD_TO_CART,
  GET_CART,
  REMOVE_FROM_CART,
  TOGGLE_DELIVERY_MODAL,
  CLOSE_MODAL,
  CLEAR_STORAGE,
  CHANGE_INSTRUCTION
} from "../actions/index";

const initialState = {
  items: [],
  cart: [],
  loading: false,
  modal: false,
  isOpen: false,
  popup: false,
  error: null,
  quantity: 1,
  price: 0,
  activeTab: 1,
  totalQuantity: 0,
  totalPrice: 0,
  editingProduct: "",
  orderType: "",
  instruction: ""
};

const products = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload.products
      };
    case FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        items: []
      };
    case TOGGLE_MODAL:
      console.log("editingg", action.payload);
      return {
        ...state,
        modal: !state.modal,
        editingProduct: action.payload,
        quantity: 1,
        instruction: ""
      };
    case CLOSE_MODAL:
      return { ...state, modal: false, isOpen: false, popup: false };
    case ADD_QUANTITY:
      return {
        ...state,
        quantity: state.quantity + 1
      };
    case REMOVE_QUANTITY:
      return {
        ...state,
        quantity: state.quantity >= 1 ? state.quantity - 1 : 0
      };
    case CHANGE_QUANTITY:
      return {
        ...state,
        quantity: action.value
      };
    case ADD_TO_CART:
      let {
        cart,
        editingProduct,
        quantity,
        modal,
        totalPrice,
        totalQuantity,
        instruction
      } = state;
      console.log("instruc", action);
      // Checks if the product is already in cart and increases it's quantity
      for (var i = 0; i < cart.length; i++) {
        if (cart[i]._id === editingProduct._id) {
          cart[i].quantity += quantity;
          cart[i].totalPrice = cart[i].quantity * cart[i].price;
          cart[i].instruction = instruction;
          const totalPArr = cart.reduce((acc, item) => {
            return acc + item.totalPrice;
          }, 0);
          let totalQ = totalQuantity + quantity;
          // localStorage.setItem('cart', JSON.stringify([...cart]));
          // localStorage.setItem('totalPrice', JSON.stringify(totalPArr));
          // localStorage.setItem('totalQuantity', JSON.stringify(totalQ));
          return {
            ...state,
            cart: [...cart],
            totalPrice: totalPArr,
            totalQuantity: totalQ,
            modal: !modal
          };
        }
      }

      // If the product is not in the cart, goes here
      editingProduct.quantity = Number(quantity);
      editingProduct.totalPrice = quantity * editingProduct.price;
      editingProduct.instruction = instruction;
      totalPrice += editingProduct.totalPrice;
      totalQuantity += Number(editingProduct.quantity);
      // localStorage.setItem('cart', JSON.stringify([...cart, editingProduct]));
      // localStorage.setItem('totalPrice', JSON.stringify(totalPrice));
      // localStorage.setItem('totalQuantity', JSON.stringify(totalQuantity));
      return {
        ...state,
        modal: !modal,
        totalPrice,
        totalQuantity,
        cart: [...cart, editingProduct]
      };
    case GET_CART:
      return {
        ...state
        // cart: [...action.cart],
        // totalPrice: action.totalPrice,
        // totalQuantity: action.totalQuantity
      };
    case REMOVE_FROM_CART:
      let newQuantity;
      let newPrice;
      const newCart = state.cart.filter(item => {
        if (item._id === action.id) {
          const quantity = item.quantity;
          const totalPrice = item.totalPrice;
          const oldQuantity = state.totalQuantity;
          const oldPrice = state.totalPrice;
          newQuantity = Number(oldQuantity) - Number(quantity);
          newPrice = Number(oldPrice) - Number(totalPrice);
          // localStorage.totalQuantity = newQuantity;
          // localStorage.totalPrice = newPrice;
        }
        return item._id !== action.id;
      });
      // localStorage.setItem('cart', JSON.stringify(newCart));
      return {
        ...state,
        cart: [...newCart],
        totalQuantity: newQuantity,
        totalPrice: newPrice
      };
    case TOGGLE_DELIVERY_MODAL:
      return {
        ...state,
        popup: !state.popup,
        orderType: action.orderType
      };
    case CLEAR_STORAGE:
      return {
        ...initialState
      };
    case CHANGE_INSTRUCTION:
      return {
        ...state,
        instruction: action.value
      };
    default:
      return state;
  }
};

export default products;
