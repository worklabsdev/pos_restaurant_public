import {
  CHECK_USER_BEGIN,
  CHECK_USER_SUCCESS,
  CHECK_USER_FAILURE,
  USER_FORM_CHANGE,
  ONLINE_ORDER_BEGIN,
  ONLINE_ORDER_SUCCESS,
  ONLINE_ORDER_FAILURE,
  RESET_ORDER,
  EDIT_USER,
  USER_DELIVER_FORM_CHANGE,
  CLEAR_STORAGE
} from "../actions";

const initialState = {
  firstName: "",
  lastName: "",
  email: "",
  phoneNo: "",
  orderType: "",
  loading: false,
  userChecked: false,
  userId: "",
  activeId: 1
};

const order = (state = initialState, action) => {
  switch (action.type) {
    case RESET_ORDER:
      return {
        ...state,
        activeId: 1
      };
    case USER_FORM_CHANGE:
      return {
        ...state,
        [action.payload.name]: action.payload.value
      };
    case USER_DELIVER_FORM_CHANGE:
      return {
        ...state,
        [action.payload.name]: action.payload.value
      };
    case EDIT_USER:
      return {
        ...state,
        userChecked: false
      };
    case CHECK_USER_BEGIN:
      return {
        ...state,
        loading: true
      };
    case CHECK_USER_SUCCESS:
      console.log("id", action.user._id);
      return {
        ...state,
        loading: false,
        userId: action.user._id,
        userChecked: true
      };
    case CHECK_USER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case ONLINE_ORDER_BEGIN:
      return {
        ...state,
        loading: true,
        orderType: action.ordertype
      };
    case ONLINE_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        otp: action.payload.otp,
        activeId: 2
      };
    case ONLINE_ORDER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case CLEAR_STORAGE:
      return { ...initialState };
    default:
      return state;
  }
};

export default order;
