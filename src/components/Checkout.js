import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { connect } from "react-redux";
import { Switch, Route } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { removeFromCart, placeOnlineOrder } from "../actions";
import Header from "./Header";
import Delivery from "./Delivery";
import Pickup from "./Pickup";
import Cart from "./Cart";

class Checkout extends Component {
  removeItemFromCart = id => {
    this.props.dispatch(removeFromCart(id));
  };

  placeOrder = () => {
    const { resId, userId, cart, totalPrice } = this.props;
    console.log("this.props.porps", cart);
    if (this.props.location.pathname === "/checkout/deliver") {
      this.setState({ orderType: "Drive Out" });
      this.props
        .dispatch(placeOnlineOrder(resId, userId, cart, totalPrice, "deliver"))
        .then(this.props.history.push("/checkout/pickup/otp"));
    } else {
      this.setState({ orderType: "Take away" });
      this.props
        .dispatch(placeOnlineOrder(resId, userId, cart, totalPrice, "pickup"))
        .then(this.props.history.push("/checkout/pickup/otp"));
    }
  };
  render() {
    console.log(this.props);
    const {
      userChecked,
      cart,
      totalPrice,
      totalQuantity,
      openDeliveryOptions,
      orderType,
      driveoutCharge
    } = this.props;

    return (
      <>
        <Header />
        <Container style={{ marginTop: "30px" }}>
          <Row>
            <Col md="8">
              {/* {orderType === 'deliver' ? <Delivery /> : <Pickup />} */}
              <Switch>
                <Route path="/checkout/deliver" component={Delivery} />
                <Route path="/checkout/pickup" component={Pickup} />
              </Switch>
            </Col>
            <Col md="4">
              <Cart
                cart={cart}
                totalPrice={totalPrice}
                totalQuantity={totalQuantity}
                openDeliveryOptions={openDeliveryOptions}
                removeItemFromCart={prodId => this.removeItemFromCart(prodId)}
                orderType={orderType}
                placeOrder={this.placeOrder}
                active={userChecked}
                driveoutCharge={driveoutCharge}
                button="Place Order"
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = ({ products, order, restaurant }) => {
  const { error, cart, totalQuantity, totalPrice } = products;
  const { userChecked, userId, activeId, orderType } = order;
  const resId = restaurant.id;
  const driveoutCharge = restaurant.driveoutCharge;

  return {
    error,
    cart,
    totalQuantity,
    totalPrice,
    orderType,
    resId,
    userChecked,
    userId,
    activeId,
    driveoutCharge
  };
};

export default withRouter(connect(mapStateToProps)(Checkout));
