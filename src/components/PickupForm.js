import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Row,
  Col,
  Label,
  Input,
  Button
} from "reactstrap";
import { checkUser, handleUserFormChange, editUser } from "../actions";
import { connect } from "react-redux";
var mailcheck = false;
var fnCheck = false;
var lnCheck = false;
var numberCheck = false;
var forminvalid = true;
const PickupForm = ({
  userChecked,

  firstName,

  lastName,
  email,
  phoneNo,
  dispatch
}) => {
  const findUser = e => {
    e.preventDefault();
    if (mailcheck == false) {
      dispatch(checkUser(email, firstName, lastName, phoneNo));
    }
  };

  const change = e => {
    const name = e.target.name;
    const value = e.target.value;
    dispatch(handleUserFormChange(name, value));
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let al = /^[a-zA-Z]+$/;
    let nu = /^[0-9]{10}$/;
    if (name == "email") {
      if (value && re.test(value) && name == "email") {
        mailcheck = false;
        forminvalid = false;
      } else {
        forminvalid = true;
        mailcheck = true;
      }
    } else if (name == "firstName") {
      if (value && al.test(value)) {
        forminvalid = false;
        fnCheck = false;
      } else {
        fnCheck = true;
        forminvalid = true;
      }
    } else if (name == "lastName") {
      if (value && al.test(value)) {
        lnCheck = false;
        forminvalid = false;
      } else {
        lnCheck = true;
        forminvalid = true;
      }
    } else if (name == "phoneNo") {
      if (value && nu.test(value)) {
        numberCheck = false;
        forminvalid = false;
      } else {
        numberCheck = true;
        forminvalid = true;
      }
    }
  };

  // const emailchange = e => {
  //   const name = e.target.name;
  //   const value = e.target.value;

  //   dispatch(handleUserFormChange(name, value));
  // };

  const edit = () => {
    dispatch(editUser());
  };

  return (
    <Card className="checkout-card">
      <CardHeader>
        Customer Details
        {userChecked && (
          <button className="link-button float-right" onClick={edit}>
            Edit
          </button>
        )}
      </CardHeader>
      <CardBody>
        <Form onSubmit={e => findUser(e)}>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="firstNname">First Name</Label>
                <Input
                  type="text"
                  name="firstName"
                  id="firstName"
                  invalid={fnCheck}
                  placeholder="First name"
                  value={firstName}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="lastNname">Last Name</Label>
                <Input
                  type="text"
                  name="lastName"
                  id="lastName"
                  invalid={lnCheck}
                  placeholder="Last name"
                  value={lastName}
                  onChange={change}
                  disabled={userChecked}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  invalid={mailcheck}
                  valid={mailcheck}
                  placeholder="Email address"
                  value={email}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="phoneNo">Phone</Label>
                <Input
                  type="text"
                  name="phoneNo"
                  id="phoneNo"
                  invalid={numberCheck}
                  placeholder="Phone number"
                  value={phoneNo}
                  onChange={change}
                  maxLength="10"
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
          </Row>
          {!userChecked && (
            <Button
              type="submit"
              disabled={forminvalid}
              color="success"
              outline
            >
              Submit
            </Button>
          )}
        </Form>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = ({ order }) => {
  const {
    email,
    phoneNo,
    firstName,
    lastName,
    userChecked,
    activeId,
    otp
  } = order;
  return { email, phoneNo, firstName, lastName, userChecked, activeId, otp };
};

export default connect(mapStateToProps)(PickupForm);
