import React, { Component } from "react"; // importing main react
import {
  ListGroup,
  ListGroupItem,
  Button,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Row,
  Col
} from "reactstrap";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";
import { connect } from "react-redux";
import {
  fetchProducts,
  toggleModal,
  addQuantity,
  removeQuantity,
  addItems,
  getCart,
  removeFromCart,
  handleQuantityChange,
  deliveryOptionsPopup,
  getRestaurantInfo,
  resetOrderPage,
  resetModal,
  handleInstructionChange
} from "../actions";
import classnames from "classnames";
import globalConst from "../helpers/global";
import Cart from "./Cart";
import Header from "./Header";

class Menu extends Component {
  componentDidMount = () => {
    const restaurantId = this.props.match.params.id;
    this.props.dispatch(resetOrderPage());
    this.props.dispatch(resetModal());
    this.props.dispatch(getCart());
    this.props.dispatch(fetchProducts(restaurantId));
    this.props.dispatch(getRestaurantInfo(restaurantId));
  };
  componentDidUpdate(prevProps, prevState) {
    console.log("MODAL STATUS", this.props.modal);
    console.log("INSTRUCTIONS", this.props.instruction);
    // var temp = "";
    // if (this.props.modal == false && prevProps.instruction) {
    //   this.props.instruction = temp;
    //   this.setState({ instruction: "" });
    // }
  }

  toggle = product => {
    this.props.dispatch(toggleModal(product));
  };

  incrementItem = () => {
    this.props.dispatch(addQuantity());
  };

  decreaseItem = () => {
    this.props.dispatch(removeQuantity());
  };

  addToCart = () => {
    this.props.dispatch(addItems());
  };

  removeItemFromCart = id => {
    this.props.dispatch(removeFromCart(id));
  };

  handleChange = e => {
    const value = Number(e.target.value);
    this.props.dispatch(handleQuantityChange(value));
  };

  openDeliveryOptions = type => {
    this.props.dispatch(deliveryOptionsPopup(type));
  };

  handleInstruction = e => {
    this.props.dispatch(handleInstructionChange(e.target.value));
  };

  render() {
    const {
      loading,
      items,
      cart,
      activeTab,
      totalQuantity,
      totalPrice,
      modal,
      editingProduct,
      quantity,
      isOpen,
      popup,
      history,
      error,
      driveoutCharge,
      instruction
    } = this.props;

    if (loading) {
      return <div>Loading...</div>;
    } else if (error) {
      return <div>Error...</div>;
    } else {
      return (
        <div>
          <Header isOpen={isOpen} toggle={this.toggle} />
          {/*  start of tabs */}
          <Container style={{ marginTop: "30px", paddingBottom: "40px" }}>
            {/* <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: activeTab === 1
                  })}
                  onClick={() => {
                    this.toggleTab(1);
                  }}
                >
                  Order
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: activeTab === 2
                  })}
                  onClick={() => {
                    this.toggleTab(2);
                  }}
                >
                  Photos
                </NavLink>
              </NavItem>
            </Nav> */}
            {/* <TabContent activeTab={activeTab} className="tab-main-element"> */}
            {/* <TabPane tabId={1}> */}
            <Row>
              <Col md="8">
                {items.length > 0 &&
                  items.map((item, index) => {
                    return (
                      <div className="product-list-element" key={item._id}>
                        <h5>{item.categoryInfo[0].name}</h5>
                        <ListGroup>
                          {item.products.map((product, index) => {
                            return (
                              <ListGroupItem
                                style={{ textAlign: "left" }}
                                key={product.itemNo}
                              >
                                <img
                                  src={`${globalConst.serverip}${
                                    product.image
                                  }`}
                                  style={{ height: "40px" }}
                                />
                                <p className="Menustyle">
                                  {product.name}
                                  <span>Kr. {product.price}</span>
                                </p>
                                <Button
                                  onClick={() => this.toggle(product)}
                                  outline
                                  color="success"
                                >
                                  Add
                                </Button>
                              </ListGroupItem>
                            );
                          })}
                        </ListGroup>
                      </div>
                    );
                  })}
              </Col>
              <Col md="4">
                <Cart
                  cart={cart}
                  totalPrice={totalPrice}
                  totalQuantity={totalQuantity}
                  openDeliveryOptions={this.openDeliveryOptions}
                  driveoutCharge={driveoutCharge}
                  removeItemFromCart={id => this.removeItemFromCart(id)}
                  button="Continue"
                />
              </Col>
            </Row>
            {/* </TabPane> */}
            {/* <TabPane tabId={2}>
                <h2>Showing Menu </h2>
                <div className="Row">
                  <div className="colMd6">hi</div>
                  <div className="colMd6">there</div>
                </div>
              </TabPane> */}
            {/* </TabContent> */}
          </Container>
          {/* End of tabs */}`
          <Modal isOpen={modal} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}>
              {editingProduct.name}
              <br />
              <span style={{ color: "grey", fontSize: "15px" }}>
                Original Price : Kr. {editingProduct.price}
              </span>
            </ModalHeader>
            <ModalBody>
              <Row>
                <span className="col-6">
                  <img
                    src={`${globalConst.serverip}${editingProduct.image}`}
                    alt={editingProduct.name}
                    style={{ maxWidth: "100px", height: "auto" }}
                  />
                </span>
                <span className="col-6">
                  <span>Instructions:</span>
                  <br />
                  <textarea
                    value={instruction}
                    onChange={e => this.handleInstruction(e)}
                    name="instruction"
                    cols="24"
                  />
                </span>
              </Row>
            </ModalBody>
            <ModalFooter>
              <div className="col-md-4">
                <div className="add_product">
                  <Button
                    onClick={this.decreaseItem}
                    outline
                    color="secondary"
                    className="minus_btn"
                  >
                    -
                  </Button>
                  <input
                    className="form-control"
                    type="text"
                    value={quantity}
                    onChange={e => this.handleChange(e)}
                  />
                  <Button
                    onClick={this.incrementItem}
                    outline
                    color="secondary"
                    className="add_btn"
                  >
                    +
                  </Button>
                </div>
              </div>

              <div className="col-md-8">
                <Button
                  color="success"
                  onClick={() => (quantity > 0 ? this.addToCart() : "")}
                  className="btn-block"
                >
                  Add {quantity} to cart{" "}
                  <span style={{ fontSize: "12px" }}>
                    [ Kr. {editingProduct.price * quantity} ]
                  </span>
                </Button>{" "}
              </div>
            </ModalFooter>
          </Modal>
          <Modal
            isOpen={popup}
            toggle={this.openDeliveryOptions}
            className={this.props.className}
          >
            <ModalHeader toggle={this.openDeliveryOptions}>
              What are you looking for?
            </ModalHeader>
            <ModalBody className="delivery-options-buttons">
              <Button
                color="success"
                onClick={() => {
                  history.push("/checkout/deliver");
                }}
              >
                Deliver Food
              </Button>{" "}
              <Button
                color="success"
                onClick={() => history.push("/checkout/pickup")}
              >
                Self Pick-up
              </Button>
            </ModalBody>
          </Modal>
        </div>
      );
    }
  }
}

const mapStateToProps = ({ products, restaurant }) => {
  const driveoutCharge = restaurant.driveoutCharge;
  const {
    activeTab,
    items,
    loading,
    error,
    cart,
    totalQuantity,
    totalPrice,
    modal,
    editingProduct,
    quantity,
    popup,
    isOpen,
    instruction
  } = products;
  console.log("state", products);
  return {
    activeTab,
    items,
    loading,
    error,
    cart,
    totalQuantity,
    totalPrice,
    modal,
    editingProduct,
    quantity,
    popup,
    isOpen,
    driveoutCharge,
    instruction
  };
};

export default connect(mapStateToProps)(Menu);
