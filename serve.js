var express = require('express');
var path = require('path');
var fs = require('fs')
const https = require('https');
const http = require('http')
const cors = require('cors')
const app = express();

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/a-board.world/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/a-board.world/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/a-board.world/chain.pem', 'utf8');

const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
};


function requireHTTPS(req, res, next) {
    if (!req.secure) {
        //FYI this should work for local development as well
        return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
}

app.use(requireHTTPS);


app.use(cors())
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));


app.use('/.well-known/acme-challenge/nr3Y9KdEptrSM_2uYhQDIux8nitcbPGucHfo9YZcTtc', express.static('/views/.well-known/acme-challenge/nr3Y9KdEptrSM_2uYhQDIux8nitcbPGucHfo9YZcTtc'))
app.use('/.well-known/acme-challenge/qzyuCSZZImlYwtrIESjQb66Rw-VPfJ-PL3mZlf71PSM', express.static('/views/.well-known/acme-challege/qzyuCSZZImlYwtrIESjQb66Rw-VPfJ-PL3mZlf71PSM'))
app.use('/',express.static('views'))
app.use('/*', function(req, res) {
    res.render('index');
});





const httpServer = http.createServer(app);
//const httpsServer = https.createServer(credentials, app);

httpServer.listen(5000, () => {
        console.log('HTTP Server running on port 80');
});

//httpsServer.listen(5001, () => {
//       console.log('HTTPS Server running on port 443');
//});





