import React from 'react';

import { Route, Switch } from 'react-router-dom';

import PickupForm from './PickupForm';
import OTP from './OTP';

const Pickup = () => {
  return (
    <>
      <Switch>
        <Route exact path="/checkout/pickup" component={PickupForm} />
        <Route exact path="/checkout/pickup/otp" component={OTP} />
      </Switch>
    </>
  );
};

export default Pickup;
