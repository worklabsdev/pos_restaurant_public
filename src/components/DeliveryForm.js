import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Row,
  Col,
  Label,
  Input,
  Button
} from "reactstrap";
import { connect } from "react-redux";
// import
import {
  checkDeliverUser,
  handleDeliveryForChange,
  editUser
} from "../actions";

var mailcheck = false;
var fnCheck = false;
var lnCheck = false;
var numberCheck = false;
var forminvalid = true;
var cityCheck = false;
var stateCheck = false;
var pinCheck = false;
var countryCheck = false;
var Router = require("react-router");
const DeliveryForm = ({
  userChecked,
  firstName,
  lastName,
  email,
  phoneNo,
  line1,
  line2,
  city,
  state,
  pin,
  country,
  dispatch
}) => {
  const findUser = e => {
    e.preventDefault();
    dispatch(
      checkDeliverUser(
        email,
        firstName,
        lastName,
        phoneNo,
        line1,
        line2,
        city,
        state,
        pin,
        country
      )
    );
  };
  const change = e => {
    // const name = e.target.name;
    // const value = e.target.value;
    // console.log("name",name);
    // console.log("value",value);
    // dispatch(handleDeliveryForChange(name,value));
    const name = e.target.name;
    const value = e.target.value;
    dispatch(handleDeliveryForChange(name, value));
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let al = /^[a-zA-Z]+$/;
    let nu = /^[0-9]{10}$/;
    let pi = /^[0-9]{3,6}$/;
    if (name == "email") {
      if (value && re.test(value) && name == "email") {
        mailcheck = false;
        forminvalid = false;
      } else {
        forminvalid = true;
        mailcheck = true;
      }
    } else if (name == "firstName") {
      if (value && al.test(value)) {
        forminvalid = false;
        fnCheck = false;
      } else {
        fnCheck = true;
        forminvalid = true;
      }
    } else if (name == "lastName") {
      if (value && al.test(value)) {
        lnCheck = false;
        forminvalid = false;
      } else {
        lnCheck = true;
        forminvalid = true;
      }
    } else if (name == "phoneNo") {
      if (value && nu.test(value)) {
        numberCheck = false;
        forminvalid = false;
      } else {
        numberCheck = true;
        forminvalid = true;
      }
    } else if (name == "city") {
      if (value && al.test(value)) {
        cityCheck = false;
        forminvalid = false;
      } else {
        cityCheck = true;
        forminvalid = true;
      }
    } else if (name == "state") {
      if (value && al.test(value)) {
        stateCheck = false;
        forminvalid = false;
      } else {
        stateCheck = true;
        forminvalid = true;
      }
    } else if (name == "pin") {
      if (value && pi.test(value)) {
        pinCheck = false;
        forminvalid = false;
      } else {
        pinCheck = true;
        forminvalid = true;
      }
    } else if (name == "country") {
      if (value && al.test(value)) {
        countryCheck = false;
        forminvalid = false;
      } else {
        countryCheck = true;
        forminvalid = true;
      }
    }
    // switch (name) {
    //   case "email":
    //     if (value && re.test(value) && name == "email") {
    //       mailcheck = false;
    //       forminvalid = false;
    //     } else {
    //       forminvalid = true;
    //       mailcheck = true;
    //     }
    //   case "firstName":
    //     if (value && al.test(value)) {
    //       forminvalid = false;
    //       fnCheck = false;
    //     } else {
    //       fnCheck = true;
    //       forminvalid = true;
    //     }
    //   case "lastName":
    //     if (value && al.test(value)) {
    //       lnCheck = false;
    //       forminvalid = false;
    //     } else {
    //       lnCheck = true;
    //       forminvalid = true;
    //     }
    //   case "phoneNo":
    //     if (value && nu.test(value)) {
    //       numberCheck = false;
    //       forminvalid = false;
    //     } else {
    //       numberCheck = true;
    //       forminvalid = true;
    //     }
    //   case "city":
    //     if (value && al.test(value)) {
    //       cityCheck = false;
    //       forminvalid = false;
    //     } else {
    //       cityCheck = true;
    //       forminvalid = true;
    //     }
    //   case "state":
    //     if (value && al.test(value)) {
    //       stateCheck = false;
    //       forminvalid = false;
    //     } else {
    //       stateCheck = true;
    //       forminvalid = true;
    //     }
    //   case "pin":
    //     if (value && pi.test(value)) {
    //       pinCheck = false;
    //       forminvalid = false;
    //     } else {
    //       pinCheck = true;
    //       forminvalid = true;
    //     }
    //   case "country":
    //     if (value && al.test(value)) {
    //       countryCheck = false;
    //       forminvalid = false;
    //     } else {
    //       countryCheck = true;
    //       forminvalid = true;
    //     }
    //   default:
    //     dispatch(handleDeliveryForChange(name, value));
    // }
  };

  const edit = () => {
    dispatch(editUser());
  };
  return (
    <Card className="checkout-card">
      <CardHeader>
        Customer Details
        {userChecked && (
          <button className="link-button float-right" onClick={edit}>
            Edit
          </button>
        )}
      </CardHeader>
      <CardBody>
        <Form onSubmit={e => findUser(e)}>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="firstNname">First Name</Label>
                <Input
                  type="text"
                  name="firstName"
                  id="firstName"
                  placeholder="First name"
                  value={firstName}
                  invalid={fnCheck}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="lastNname">Last Name</Label>
                <Input
                  type="text"
                  name="lastName"
                  id="lastName"
                  placeholder="Last name"
                  value={lastName}
                  invalid={lnCheck}
                  onChange={change}
                  disabled={userChecked}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email address"
                  value={email}
                  invalid={mailcheck}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="phoneNo">Phone</Label>
                <Input
                  type="text"
                  name="phoneNo"
                  id="phoneNo"
                  placeholder="Phone number"
                  value={phoneNo}
                  invalid={numberCheck}
                  onChange={change}
                  maxLength="10"
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>

            <Col md={12}>
              <h3>Address</h3>
            </Col>
            <hr />

            <Col md={6}>
              <FormGroup>
                <Label for="line1">Line 1</Label>
                <Input
                  type="text"
                  name="line1"
                  id="line1"
                  placeholder="Line 1"
                  value={line1}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="address">Line 2</Label>
                <Input
                  type="text"
                  name="line2"
                  id="line2"
                  placeholder="Line 2"
                  value={line2}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>

            <Col md={6}>
              <FormGroup>
                <Label for="address">City</Label>
                <Input
                  type="text"
                  name="city"
                  id="city"
                  placeholder="City"
                  value={city}
                  invalid={cityCheck}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>

            <Col md={6}>
              <FormGroup>
                <Label for="address">State</Label>
                <Input
                  type="text"
                  name="state"
                  id="state"
                  placeholder="State"
                  value={state}
                  invalid={stateCheck}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>

            <Col md={6}>
              <FormGroup>
                <Label for="address">Pin</Label>
                <Input
                  type="text"
                  name="pin"
                  id="pin"
                  placeholder="Pin"
                  value={pin}
                  invalid={pinCheck}
                  maxLength="5"
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>

            <Col md={6}>
              <FormGroup>
                <Label for="address">Country</Label>
                <Input
                  type="text"
                  name="country"
                  id="country"
                  placeholder="Country"
                  value={country}
                  invalid={countryCheck}
                  onChange={change}
                  disabled={userChecked}
                  required
                />
              </FormGroup>
            </Col>
          </Row>

          {!userChecked && (
            <Button
              type="submit"
              disabled={forminvalid}
              color="success"
              outline
            >
              Submit
            </Button>
          )}
        </Form>
      </CardBody>
    </Card>
  );
};

const mapStatetoProps = ({ order }) => {
  const {
    email,
    phoneNo,
    firstName,
    lastName,
    line1,
    line2,
    city,
    state,
    pin,
    country,
    userChecked,
    activeId,
    otp
  } = order;
  return {
    email,
    phoneNo,
    firstName,
    lastName,
    line1,
    line2,
    city,
    state,
    pin,
    country,
    userChecked,
    activeId,
    otp
  };
};

export default connect(mapStatetoProps)(DeliveryForm);
