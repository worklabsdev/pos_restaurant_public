import React from "react";
import { Card, CardHeader, CardBody, Button } from "reactstrap";
import { connect } from "react-redux";
import { clearStorage } from "../actions";

const OTP = ({ otp, dispatch, history, id }) => {
  const clearState = () => {
    dispatch(clearStorage());
    history.push(`/restaurant/${id}`);
  };
  return (
    <Card className="checkout-card">
      <CardHeader>Order Details</CardHeader>
      <CardBody className="otp-card">
        <div className="checkmark-circle">
          <div className="background" />
          <div className="checkmark draw" />
        </div>
        <div>
          <h1 className="display-4">
            <strong>OTP: </strong>
            {otp}
          </h1>
          <p>
            You need to provide OTP at the restaurant to collect your order.
            <br />
            We have also send the OTP to your email address.
          </p>
          <Button color="success" outline onClick={clearState}>
            Place New Order
          </Button>
        </div>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = ({ order, restaurant }) => {
  return {
    otp: order.otp,
    id: restaurant.id,
    orderType: order.orderTypes
  };
};

export default connect(mapStateToProps)(OTP);
