import {
  GET_RESTAURANT_BEGIN,
  GET_RESTAURANT_SUCCESS,
  GET_RESTAURANT_FAILURE
} from '../actions/index';

const initialState = {
  error: null,
  loading: false,
  name: '',
  logo: '',
  email: '',
  phoneNo: ''
};

const restaurant = (state = initialState, action) => {
  switch (action.type) {
    case GET_RESTAURANT_BEGIN:
      return {
        ...state,
        loading: true
      };
    case GET_RESTAURANT_SUCCESS:
      return {
        ...state,
        email: action.data.email,
        name: action.data.name,
        logo: action.data.logo,
        phoneNo: action.data.phoneNo,
        id: action.data._id,
        driveoutCharge: action.data.driveoutCharge,
        loading: false
      };
    case GET_RESTAURANT_FAILURE:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};

export default restaurant;
